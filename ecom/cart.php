<?php
	require 'src/Shop.php';
    require 'src/Account.php';
    $account = new Account;	
	$shop = new Shop;

	if(isset($_SESSION['user_id']) && isset($_SESSION['logined']) && isset($_SESSION['user_type'])){
	}else{
		header('location: acc_login.php');
	}

	if(isset($_POST['update_cart'])){
		// echo "<pre>";
		// print_r($_POST);
		$id = $_POST['update_cart'];
		$shop->update_cart($id, $_POST['prod_qty'][$id]);
		
	}

	if(isset($_POST['remove_cart'])){
		// echo "<pre>";
		// print_r($_POST);
		$id = $_POST['remove_cart'];
		$shop->remove_cart($id);
		
	}
	

	$cart = $shop->get_cart_items($_SESSION['user_id']);

?>

<?php require 'section_head.php';?>
<?php require 'section_header_main.php';?>


<!-- SECTION -->
<main class="section">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<h2 class="mt-4">Cart</h2>
		<div class="row overflow-auto">
			<table class="table">
				<tr>
					<td>Product</td>
					<td>Qty</td>
					<td></td>
					<td>Price</td>
					<td>Total</td>
					<td></td>
				</tr>
				<?php
						if(empty($cart)){
							echo '<tr>
							<td class="text-center" colspan="6">No items in cart</td>
							
						</tr>';
						}
						$total = 0;
						$grand_total = 0;
						$checkout = true;
						foreach ($cart as $key => $item) {
							$product = $shop->get_product($item['product_id']);
							$total = ($product['price_selling'] * $item['qty']);
							$grand_total += $total;
							$stock = '';
							if ($product['stock_qty'] < $item['qty']) {
								$stock = ' - ( Out of stock )';
								$checkout = false;
							}
							echo '<tr>
									<form action="" method="post">
										<td>'.$product['prod_name'].$stock.'</td>
										<td><div class="input-number">
												<input type="hidden" value="'.$item['cart_item_id'].'" name="cart_item_id['.$item['cart_item_id'].']">
												<input type="number" value="'.$item['qty'].'" name="prod_qty['.$item['cart_item_id'].']" >
											</div></td>
										<td>
										<button class="btn" type="submit"  value="'.$item['cart_item_id'].'" name="update_cart">Update</button></td>
										<td>'.$product['price_selling'].'</td>
										<td>'.$total.'</td>
										<td><button class="btn btn-danger" type="submit" value="'.$item['cart_item_id'].'" name="remove_cart">Remove</button></td>
									</form>
								</tr>';
						}

						?>

				<tr>
					<td colspan="4">Total</td>
					<td colspan="2"><strong><?=$grand_total?></strong></td>
				</tr>
			</table>
		</div>
		<!-- /row -->
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4"><a href="checkout.php"
					class="btn btn-success <?=($checkout)?'':'disabled';?>">Checkout</a></div>
			<div class="col-md-4"></div>
		</div>
	</div>
	<!-- /container -->
</main>

<?php require 'section_footer.php';?>
