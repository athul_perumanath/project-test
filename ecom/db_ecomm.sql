-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 31, 2020 at 01:02 AM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ecomm`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
CREATE TABLE IF NOT EXISTS `cart` (
  `cart_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(10) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`cart_item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`cart_item_id`, `user_id`, `product_id`, `qty`, `date_added`) VALUES
(6, 6, 61, 1, '2020-05-30 15:44:56'),
(7, 6, 63, 1, '2020-05-30 17:28:28'),
(8, 6, 63, 1, '2020-05-30 17:28:30');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(50) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category_name`, `parent_id`) VALUES
(13, 'Home audio', 0),
(14, 'Wireless', 0),
(15, 'Headphone', 0),
(34, 'Bluetooth', 14);

-- --------------------------------------------------------

--
-- Table structure for table `ecom_users`
--

DROP TABLE IF EXISTS `ecom_users`;
CREATE TABLE IF NOT EXISTS `ecom_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `user_type` varchar(50) NOT NULL DEFAULT 'user',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ecom_users`
--

INSERT INTO `ecom_users` (`user_id`, `username`, `password`, `email`, `full_name`, `user_type`) VALUES
(1, 'admin', '$2y$10$0W5t0epmFnT5OixZ3LE.UOG/NUL2S.46LrTJhrA1kqzMhzGpuR206', 'admin@bizcaresolution.in', 'Admin', 'admin'),
(2, 'user', '$2y$10$kaDHRpqxZRUxBfn9HZ7Nau1V6EgJq2T0jFvYEwqJZNdR2RHkY0fmK', 'athul@gmail.com', 'Athul', 'user'),
(3, 'user@gmail.com', '$2y$10$kjTRPyuYmr7nN3xrXKkRHeCHtc4VisDB/ehNTe9Q.2d4u7EVfNFVi', 'user@gmail.com', 'user1', 'user'),
(5, 'athulrajd@gmail.com', '$2y$10$f8rMjDm5E92koyNf9a3Rp.zhGBqWJILStWGHIVNSIA/Quylo/nYTu', 'athulrajd@gmail.com', 'Athulraj', 'admin'),
(6, 'nivinkjohnson@gmail.com', '$2y$10$.UwtSvEAFsl5KIIZ18Yp/.vmrseye0uCsjHL0HNnn/rvQlF9lxQmG', 'nivinkjohnson@gmail.com', 'Nivin', 'user');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `order_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delivery_date` date DEFAULT NULL,
  `order_total` int(11) NOT NULL,
  `shipping_charge` int(11) NOT NULL,
  `shipping_method` varchar(50) NOT NULL,
  `order_status` varchar(50) NOT NULL,
  `tracking_id` varchar(255) NOT NULL,
  `delivery_address` text NOT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `user_id`, `order_date`, `delivery_date`, `order_total`, `shipping_charge`, `shipping_method`, `order_status`, `tracking_id`, `delivery_address`) VALUES
(1, 1, '2019-06-10 16:04:16', '0000-00-00', 1640, 50, 'Express', 'shipped', '', 'Edathamaramana Shopping Complex, Kadavanthra, , 5464997664'),
(2, 5, '2020-05-18 00:21:02', '0000-00-00', 1490, 50, 'Express', 'pending', '', 'sadsadsa , 732467813246821734'),
(3, 1, '2020-05-24 00:24:54', NULL, 98050, 50, 'Express', 'pending', '', 'HSDFK , JKSDHK'),
(4, 1, '2020-05-24 00:29:59', NULL, 99750, 50, 'Express', 'shipped', '', 'jhsjkd , 32784632874'),
(5, 1, '2020-05-24 15:13:34', NULL, 190, 50, 'Express', 'pending', '', 'yest , 676787'),
(6, 1, '2020-05-24 15:16:15', NULL, 190, 50, 'Express', 'pending', '', 'jhjkh , 456456'),
(7, 1, '2020-05-24 15:17:23', NULL, 190, 50, 'Express', 'pending', '', 'jhjkh , 456456'),
(8, 1, '2020-05-24 15:18:36', NULL, 330, 50, 'Express', 'canceled', '', 'jhsdjkf37824678 , 87346328974'),
(9, 1, '2020-05-24 15:19:05', NULL, 330, 50, 'Express', 'processing', '9', 'cgvg , 456456');

-- --------------------------------------------------------

--
-- Table structure for table `order_item`
--

DROP TABLE IF EXISTS `order_item`;
CREATE TABLE IF NOT EXISTS `order_item` (
  `order_items_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `selling_price` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  PRIMARY KEY (`order_items_id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_item`
--

INSERT INTO `order_item` (`order_items_id`, `order_id`, `product_id`, `selling_price`, `quantity`, `total`) VALUES
(1, 1, 27, 950, 1, 950),
(2, 2, 27, 950, 1, 950),
(3, 2, 31, 250, 1, 250),
(4, 1, 50, 530, 3, 1590),
(5, 2, 58, 180, 4, 720),
(6, 2, 58, 180, 4, 720),
(7, 3, 61, 980, 100, 98000),
(8, 4, 61, 980, 100, 98000),
(9, 4, 61, 980, 1, 980),
(10, 4, 58, 180, 1, 180),
(11, 4, 58, 180, 1, 180),
(12, 4, 58, 180, 1, 180),
(13, 4, 58, 180, 1, 180),
(14, 5, 62, 140, 1, 140),
(15, 6, 62, 140, 1, 140),
(16, 7, 62, 140, 1, 140),
(17, 8, 62, 140, 1, 140),
(18, 8, 62, 140, 1, 140),
(19, 9, 62, 140, 1, 140),
(20, 9, 62, 140, 1, 140),
(21, 0, 61, 980, 1, 980),
(22, 0, 63, 350, 1, 350),
(23, 0, 63, 350, 1, 350),
(24, 0, 63, 350, 1, 350),
(25, 0, 61, 980, 1, 980),
(26, 0, 63, 350, 1, 350),
(27, 0, 63, 350, 1, 350),
(28, 0, 63, 350, 1, 350),
(29, 0, 61, 980, 1, 980),
(30, 0, 63, 350, 1, 350),
(31, 0, 63, 350, 1, 350),
(32, 0, 63, 350, 1, 350),
(33, 0, 61, 980, 1, 980),
(34, 0, 63, 350, 1, 350),
(35, 0, 63, 350, 1, 350),
(36, 0, 63, 350, 1, 350),
(37, 0, 61, 980, 1, 980),
(38, 0, 63, 350, 1, 350),
(39, 0, 63, 350, 1, 350),
(40, 0, 63, 350, 1, 350),
(41, 0, 61, 980, 1, 980),
(42, 0, 63, 350, 1, 350),
(43, 0, 63, 350, 1, 350),
(44, 0, 61, 980, 1, 980),
(45, 0, 63, 350, 1, 350),
(46, 0, 63, 350, 1, 350);

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
CREATE TABLE IF NOT EXISTS `payments` (
  `transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `payment_ref` varchar(100) NOT NULL,
  `payment_status` varchar(50) NOT NULL DEFAULT 'pending',
  `amount` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`transaction_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`transaction_id`, `order_id`, `date`, `payment_ref`, `payment_status`, `amount`) VALUES
(1, 1, '2019-06-08 01:35:05', '', 'success', 1000),
(2, 2, '2019-06-08 01:35:44', '', 'failed', 1250),
(3, 1, '2019-06-10 16:04:16', '', 'success', 1640),
(4, 2, '2020-05-18 00:21:03', '', 'success', 1490),
(5, 3, '2020-05-24 00:24:54', '', 'success', 98050),
(6, 4, '2020-05-24 00:30:00', '', 'success', 99750),
(7, 5, '2020-05-24 15:13:34', '', 'success', 190),
(8, 6, '2020-05-24 15:16:15', '', 'pending', 190),
(9, 7, '2020-05-24 15:17:24', '', 'pending', 190),
(10, 8, '2020-05-24 15:18:36', '', 'success', 330),
(11, 9, '2020-05-24 15:19:05', '', 'success', 330),
(12, 0, '2020-05-30 17:48:42', 'cod', 'success', 2080),
(13, 0, '2020-05-30 17:51:03', 'cod', 'success', 2080),
(14, 0, '2020-05-31 05:33:58', 'cod', 'success', 2080),
(15, 0, '2020-05-31 05:34:49', 'debit/credit', 'success', 2080);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `prod_id` int(11) NOT NULL AUTO_INCREMENT,
  `prod_name` varchar(255) NOT NULL,
  `prod_des` text NOT NULL,
  `price_mrp` int(11) NOT NULL DEFAULT '0',
  `price_selling` int(11) NOT NULL DEFAULT '0',
  `prod_img` text NOT NULL,
  `stock_qty` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`prod_id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`prod_id`, `prod_name`, `prod_des`, `price_mrp`, `price_selling`, `prod_img`, `stock_qty`, `deleted`) VALUES
(61, 'JBL Home audio', '\r\n', 1000, 980, 'uploads/61.jpg', 292, 0),
(62, 'Sony Wireless neckband', '', 150, 140, 'uploads/62.jpg', 193, 0),
(63, 'JBL headphone 2520', '', 400, 350, 'uploads/63.jpg', 381, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_cat_assign`
--

DROP TABLE IF EXISTS `product_cat_assign`;
CREATE TABLE IF NOT EXISTS `product_cat_assign` (
  `combination_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`combination_id`)
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_cat_assign`
--

INSERT INTO `product_cat_assign` (`combination_id`, `product_id`, `category_id`) VALUES
(117, 61, 13),
(118, 62, 14),
(119, 63, 15),
(121, 62, 13);

-- --------------------------------------------------------

--
-- Table structure for table `threads`
--

DROP TABLE IF EXISTS `threads`;
CREATE TABLE IF NOT EXISTS `threads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `threads`
--

INSERT INTO `threads` (`id`, `title`, `user_id`) VALUES
(10, 'What are the available shipping locations', 1),
(12, 'What are the return policies?', 1),
(13, 'How many products are there for JBL headphones?', 1),
(14, 'What is the regular shipping cost for products?', 6),
(17, '', 6);

-- --------------------------------------------------------

--
-- Table structure for table `thread_replies`
--

DROP TABLE IF EXISTS `thread_replies`;
CREATE TABLE IF NOT EXISTS `thread_replies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `thread_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `reply_message` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `thread_replies`
--

INSERT INTO `thread_replies` (`id`, `thread_id`, `user_id`, `reply_message`) VALUES
(12, 8, 1, 'lk'),
(13, 8, 1, 'hghj'),
(14, 14, 1, '50 Dollars per order'),
(15, 14, 6, 'test');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
