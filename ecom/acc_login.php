<?php
    require 'src/Shop.php';
    require 'src/Account.php';
    $shop = new Shop;
    $account = new Account;

    
    $error = false;
    if(isset($_POST['login'])){
        if(!$account->login()){
            $error = true;
        }else{
            if($_SESSION['user_type'] == 'admin'){
                header('location: index.php');
            }
            else {
                header('location: index.php');
            }
        }
    }
    if(isset($_POST['register'])){
        $message = $account->user_registration();
    }

    /* if(isset($_SESSION['user_id']) && isset($_SESSION['logined']) && isset($_SESSION['user_type'])){
        if($_SESSION['user_type'] == 'admin'){
            header('location: admin/order_pending.php');
        }else{
            header('location: index.php');
        }
    }
 */
?>

<?php require 'section_head.php';?>
<?php require 'section_header_main.php';?>

<main class="section">
    <!-- container -->
    <div class="container">
        <div class="row mt-4">
            <div class="col-sm-5 col-sm-offset-1">
                <div class="login-form">
                    <!--login form-->
                    <h2 class="mb-4">Login to your account</h2>
                    <form action="" method="post">
                        <p>
                            <input type="text" size="20" placeholder="Email" name="username" class="input" required>
                        </p>
                        <p>
                            <input type="password" size="20" placeholder="Password" name="password" class="input" required>
                        </p>
                        <!-- <p><a href="#">Forgot Password</a></p> -->
                        <button type="submit" name="login" class="btn btn-default">Login</button>
                        <?php 
                            if($error){
                                echo '<p class="text-danger">Invalid credentials!</p>';
                            }
                        ?>
                    </form>
                </div>
                <!--/login form-->
            </div>
            <div class="col-sm-1">
                <h2 class="or">OR</h2>
            </div>
            <div class="col-sm-6">
                <div class="signup-form">
                    <!--sign up form-->
                    <h3 class="mb-4">New User Signup!</h3>
                        <form action="" method="post">
                            <p>
                                <input type="text" placeholder="Name" name="full_name" class="input" required>
                            </p>
                            <p>
                                <input type="email" placeholder="Email Address" name="email" class="input" required>
                            </p>
                            <p>
                                <input type="password" placeholder="Password" name="password" class="input" required>
                            </p>
                            <button type="submit" name="register" class="btn btn-default">Signup</button>
                        </form>
                    <br>
                    <h5><?=@$message?></h5>
                </div>
                <!--/sign up form-->
            </div>
        </div>
    </div>
    <!-- /container -->
</main>
<!-- /SECTION -->

<!-- FOOTER -->
<?php require 'section_footer.php';?>