<?php
    require 'src/Account.php';	
	require 'src/Shop.php';
    $account = new Account;	
	$shop = new Shop;

	
	if(isset($_SESSION['user_id']) && isset($_SESSION['logined']) && isset($_SESSION['user_type'])){
		$user_logined = true;
		if(isset($_POST['add_thread'])){
			$shop->add_thread($_POST['thread'], $_SESSION['user_id']);
			
		}
	}else{
		$user_logined = false;
		
	}


	$threads = $shop->get_all_threads();

?>

<?php include('./section_head.php'); ?>
<?php include('./section_header_main.php'); ?>


<!-- /NAVIGATION -->

<!-- container -->
<main class="container">
	<!-- row -->
	<div class="row">
		<div class="col-sm-6 mt-2">
			<h2 class="mt-3">Forum</h2>
			<!-- Product main img -->
			<ul class="list-group ">
				<?php foreach($threads as $key => $thread){?>
				<li class="list-group-item d-flex justify-content-between align-items-center">
					<a href="forum_thread.php?thread=<?php echo $thread['id'] ?>">
						<?php echo $thread['title'] ?>
					</a>
					<span class="badge badge-primary badge-pill"><?php echo $thread['user']['full_name'] ?></span>
				</li>
				<?php } ?>


			</ul>
		</div>
		<?php if($user_logined){ ?>
		<div class="col-sm-6 mt-2">
			<h2 class="mt-3">Add new thread</h2>

			<form action="" method="POST">
				<div class="row">
					<div class="col-12">
					</div>
				</div>
				<div class="form-group">
					<input type="text" name="thread" placeholder="Enter Content" class="input">
				</div>
				<button type="submit" class="btn pull-right" name="add_thread">Add new thread</button>
			</form>

		</div>
		<?php } ?>




	</div>
	<!-- /row -->
</main>
<!-- /container -->

<!-- FOOTER -->
<?php require 'section_footer.php';?>
<!-- jQuery Plugins -->