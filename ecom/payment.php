<?php
    require 'src/Shop.php';
    $shop = new Shop;

	if(isset($_POST['txn'])){
		$payment_id = $_GET['payment_id'];
		$order_id = $shop->order_id_payment_id($payment_id);
		if($_POST['txn'] == 'yes'){
			$shop->txn_update($_GET['payment_id'], 'success');
			echo '<h1>Transaction success</h1><a class="btn btn-success" href="index.php">Go to home</a>';
			header('location: index.php');
		}else{
			$shop->txn_update($_GET['payment_id'], 'failed');
			$shop->order_update($order_id, 'failed');
			$shop->order_rollback($order_id);
			echo '<h1>Transaction failed</h1><a class="btn btn-danger" href="index.php">Go to home</a>';
			exit;
		}
		
	  }
?>
<!DOCTYPE html>
<html lang="en">
<!-- HEAD -->
<?php require 'section_head.php';?>
<!-- /HEAD -->
	<body>
	<H1>Payment status</H1>
		<div>
			<form action="" method="post">
				<button type="submit" name="txn" value="yes" class="btn btn-success">Complete payment</button>
				<!-- <button type="submit" name="txn" value="no" class="btn btn-danger">failed transaction</button> -->
			</form>
		</div>
		

	</body>
</html>
