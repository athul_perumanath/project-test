<?php
require '../src/Account.php';
require '../src/Admin.php';

$account = new Account;
$admin = new Admin;
$sales = $admin->report_sales();
?>

<!DOCTYPE html>
<html lang="en">

<?php
  require_once 'section_head.php';
?>

<body>

  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <?php
      require_once 'section_sidebar.php';
    ?>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
      <?php
        require_once 'section_navbar.php';
      ?>


      <div class="container-fluid p-3">
        <h2 class="mt-2">Sales report</h2>

        <table class="table table-bordered mt-4">
            <thead>
              <tr>
                <th scope="col">Sl no.</th>
                <th scope="col">Date</th>
                <th scope="col">Amount</th>
              </tr>
            </thead>
            <tbody>
            <?php
            $total = 0;
            foreach ($sales as $key => $sale) {
              echo '<tr>
                <th scope="row">'.($key + 1).'</th>
                <td>'.$sale['date'].'</td>
                <td>'.$sale['amount'].'</td>
              </tr>';
            $total += $sale['amount'];
            }
            echo '<tr>
              <td colspan="2">Total sales </td>
              <th>'.$total.'</th>
            </tr>';
            ?>
            </tbody>
          </table>
      </div>



    </div>
    <!-- /#page-content-wrapper -->

  </div>
  <!-- /#wrapper -->

    <?php
        require_once 'section_navbar.php';
      ?>

</body>

</html>
