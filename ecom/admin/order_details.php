<?php
require '../src/Account.php';
require '../src/Admin.php';

$account = new Account;
$admin = new Admin;
// echo "<pre>";

$message = '';
if(isset($_POST['order_status'])){
  if($admin->update_order($_GET['order_id'])){
    $message = "Order Updated!";
  }
}

if (!isset($_GET['order_id'])) {
  header('location: order_pending.php');
}
if(!$order = $admin->get_order($_GET['order_id'])){
  header('location: order_pending.php');
}
// print_r($order);

?>


<!DOCTYPE html>
<html lang="en">

<?php
  require_once 'section_head.php';
?>
<body>

  <div class="d-flex" id="wrapper">
    <!-- Sidebar -->
    <?php
      require_once 'section_sidebar.php';
    ?>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

        <?php
          require_once 'section_navbar.php';
        ?>

        <div class="container-fluid p-3">
          <h2 class="mt-2">Order details</h2>
          <p><?=$message?></p>
          <div class="row p-2">
            <div class="col-md-4 p-1">
              <div class="card">
                <div class="card-header">Order details: (<?=$order['order_status']?>)</div>
                <div class="card-body list-group list-group-flush p-0">
                      <div class="list-group-item">Date: <?=$order['order_date']?> </div>
                      <div class="list-group-item">Delivery date: <?=$order['delivery_date']?> </div>
                      <div class="list-group-item">Tracking ID: <?=$order['tracking_id']?> </div>
                </div>
              </div>
            </div>
            <div class="col-md-4 p-1">
              <div class="card">
                <div class="card-header">Customer details: </div>
                <div class="card-body list-group list-group-flush p-0">
                  <div class="list-group-item">Name: <?=$order['customer']?> </div>
                  <div class="list-group-item">Address: <?=$order['delivery_address']?> </div>
                </div>
              </div>
            </div>
            <div class="col-md-4 p-1">
              <div class="card">
                <div class="card-header"> Payment: </div>
                <div class="card-body list-group list-group-flush p-0">
                  <div class="list-group-item">Total: <?=$order['payment']['amount']?> ( <?=$order['payment']['payment_status']?> )</div>
                  <div class="list-group-item">Payment date: <?=$order['payment']['date']?></div>
                  <div class="list-group-item">Payment Mode: <?=$order['payment']['payment_ref']?></div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">Order items</div>
                <div class="card-body">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th scope="col">Product Name</th>
                          <th scope="col">Quantity</th>
                          <th scope="col">Unit price</th>
                          <th scope="col">Total</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php 
                      $sub_total = 0;
                        foreach ($order['order_items'] as $key => $item) {
                          echo '<tr>
                                  <td>'.$item['product_name'].'</td>
                                  <td>'.$item['quantity'].'</td>
                                  <td>$'.$item['selling_price'].'</td>
                                  <td>$'.$item['total'].'</td>
                                </tr>';
                                $sub_total += $item['total'];
                        }
                      ?>
                        
                        <tr>
                          <td colspan="3" class="text-right">Sub total</td>
                          <td><?=$sub_total?></td>
                        </tr>
                        <tr>
                          <td colspan="3" class="text-right">Shipping charge</td>
                          <td><?=$order['shipping_charge']?></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="text-right"> Total</td>
                            <td><strong><?=$order['order_total']?></strong></td>
                          </tr>
                      </tbody>
                    </table>
                </div>
              </div>
            </div>
            
          </div>
          <div class="row mt-3 mb-5">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">Order status</div>
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6">
                        <form class="" action="" method="POST">
                          <div class="form-row ">
                            <div class="form-group col-md-12">
                                <label for="inputState">Order status</label>
                                <select id="inputState" name="order_status" class="form-control">
                                <?php
                                    $status_all = array(
                                        "pending" => "Pending",
                                        "shipped" => "Shipped",
                                        "processing" =>"Processing",
                                        "completed" => "Completed",
                                        "canceled" => "Canceled",
                                        "return" => "Return",
                                        "refunded" => "Refunded"
                                      );
                                    foreach ($status_all as $value => $name) {
                                      $selection = ($value == $order['order_status'])?'selected':'';
                                      echo '<option '.$selection.' value="'.$value.'">'.$name.'</option>';
                                    }
                                ?>
                                </select>
                            </div>
                            
                            <div class="form-group col-md-12">
                                <label for="trackingId">Tracking id</label>
                                <input type="text" class="form-control" value="<?=$order['tracking_id']?>" name="tracking_id" id="trackingId" placeholder="Tracking id of shipment">
                            </div>
                          </div>
                          <button type="submit" class="btn btn-success">Update order status</button>
                        </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
    </div>
    <!-- /#page-content-wrapper -->
  </div>
  <!-- /#wrapper -->

  <?php
    require_once 'section_footer.php';
  ?>
  
</body>
</html>
