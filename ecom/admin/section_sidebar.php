<!-- Sidebar -->
<div class="bg-light border-right d-print-none" id="sidebar-wrapper">
    <div class="sidebar-heading text-center"><a href="index.php"><strong>E-Music</strong></a></div>
    <ul class="list-group">
        
        <li class="list-group-item d-flex justify-content-between align-items-center list-group-item-info">
            <strong>Orders</strong>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center ">
            <a href="order_pending.php" class="">Pending orders</a>
            <!-- <span class="badge badge-primary badge-pill"></span> -->
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center ">
            <a href="order_processing.php" class="">Processing orders</a>
            <!-- <span class="badge badge-primary badge-pill">14</span> -->
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center ">
            <a href="order_completed.php" class="">Completed orders</a>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center ">
            <a href="order_cancelled.php" class="">Cancelled orders</a>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center list-group-item-info ">
            <strong>Products</strong>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center ">
            <a href="products_list_all.php" class="">All products</a>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center ">
            <a href="products_add.php" class="">Add product</a>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center list-group-item-info">
            <strong>Categories</strong>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center ">
            <a href="category_all.php" class="">All  categories</a>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center list-group-item-info">
            <strong>Users</strong>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center ">
            <a href="user_list_all.php" class="">All users</a>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center ">
            <a href="user_add.php" class="">Add user</a>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center list-group-item-info">
            <strong>Reports</strong>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center ">
            <a href="report_sales.php" class="">Sales</a>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center ">
            <a href="report_stock.php" class="">Stock</a>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center list-group-item-info">
            <strong>Forum</strong>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center ">
            <a href="forum_threads.php" class="">All threads</a>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center ">
            <!-- <a href="forum_threads.php" class="">s</a> -->
        </li>
        
    </ul>
</div>

<!-- /#sidebar-wrapper -->