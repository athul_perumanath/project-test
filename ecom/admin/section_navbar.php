<nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
    
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item active">
            <a class="nav-link" href="../index.php" >Visit store <span class="sr-only">(current)</span></a>
            </li>
            
            <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Account
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <!-- <a class="dropdown-item" href="#">Password change</a> -->
                <!-- <div class="dropdown-divider"></div> -->
                <a class="dropdown-item" href="../acc_logout.php">Logout</a>
            </div>
            </li>
        </ul>
    </div>
</nav>