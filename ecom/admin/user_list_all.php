<?php
require '../src/Account.php';
require '../src/Admin.php';

$account = new Account;
$admin = new Admin;
$users = $admin->get_users_list();
// $account->login_action()
?>

<!DOCTYPE html>
<html lang="en">

<?php
  require_once 'section_head.php';
?>

<body>
  <div class="d-flex" id="wrapper">
    <!-- Sidebar -->
    <?php
      require_once 'section_sidebar.php';
    ?>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
      <?php
        require_once 'section_navbar.php';
      ?>

      <div class="container-fluid p-3">
        <h2 class="mt-2">Users</h2>

        <table class="table table-bordered mt-4 ">
            <thead >
              <tr>
                <th scope="col">#</th>
                <th scope="col">full_name</th>
                <th scope="col">Username</th>
                <th scope="col">email</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
            <?php
            if(empty($users)){
              echo '<tr><td class="text-center" colspan="5">No records found</td></tr>';
            }
            foreach ($users as $key => $user) {
              echo '<tr>
                      <th scope="row">'.$user['user_id'].'</th>
                      <td>'.$user['full_name'].'</td>
                      <td>'.$user['username'].'</td>
                      <td>'.$user['email'].'</td>
                      <td>
                        <a href="user_edit.php?user_id='.$user['user_id'].'"><button type="button" class="btn btn-primary btn-sm">Manage user</button></a>
                      </td>
                    </tr>';
                    
            }
            ?>
              
             
            </tbody>
          </table>
      </div>
    </div>
    <!-- /#page-content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <?php
    require_once 'section_footer.php';
  ?>

</body>

</html>
