<?php
require '../src/Account.php';
require '../src/Admin.php';

$account = new Account;
$admin = new Admin;
// echo "<pre>";
$categories = $admin->get_category_list();
// print_r($categories);
?>
<!DOCTYPE html>
<html lang="en">

<?php
  require_once 'section_head.php';
?>

<body>

  <div class="d-flex" id="wrapper">

  <?php
    require_once 'section_sidebar.php';
  ?>

    <!-- Page Content -->
    <div id="page-content-wrapper">

      <?php
        require_once 'section_navbar.php';
      ?>
      <div class="container-fluid p-3">
        <h2 class="mt-2"><span>Categories<a href="category_add.php"><button class="ml-3 btn btn-success pull-right">Add new</button></a></span> </h2>
        <table class="table table-bordered mt-4">
            <thead>
              <tr>
                <th scope="col">Category name</th>
                <th scope="col" class="text-center">Actions</th>
              </tr>
            </thead>
            <tbody>
            <?php
            foreach ($categories as $key => $category) {
              echo '<tr>
                      <td class="">'.$category['category_name'].'</td>
                      <td class="text-center">
                        <a href="	category_edit.php?category_id='.$category['category_id'].'"><button type="button" class="btn btn-primary btn-sm">Edit Category</button></a>
                      </td>
                    </tr>';
            }
            ?>
            </tbody>
          </table>
      </div>
    </div>
    <!-- /#page-content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <?php
    require_once 'section_footer.php';
  ?>

</body>

</html>
