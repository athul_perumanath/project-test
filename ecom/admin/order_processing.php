<?php
require '../src/Account.php';
require '../src/Admin.php';

$account = new Account;
$admin = new Admin;
$orders = $admin->get_order_list('processing');


?>


<!DOCTYPE html>
<html lang="en">

<?php
  require_once 'section_head.php';
?>

<body>

  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <?php
    require_once 'section_sidebar.php';
  ?>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

      <?php
        require_once 'section_navbar.php';
      ?>


      <div class="container-fluid p-3">
        <h2 class="mt-2">Processing orders</h2>
        
        <table class="table table-bordered mt-4 ">
            <thead >
              <tr>
                <th scope="col">Order ID</th>
                <th scope="col">Customer</th>
                <th scope="col">Date</th>
                <th scope="col">Total</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
              
            <?php
            if(empty($orders)){
              echo '<tr><td class="text-center" colspan="5"><strong>No records found</strong></td></tr>';
            }
            foreach ($orders as $key => $order) {
              echo '<tr>
                      <th scope="row">'.$order['order_id'].'</th>
                      <td>'.$admin->get_user($order['user_id'])['full_name'].'</td>
                      <td>'.$order['order_date'].'</td>
                      <td>'.$order['order_total'].'</td>
                      <td>
                        <a href="order_details.php?order_id='.$order['user_id'].'"><button type="button" class="btn btn-primary btn-sm">Manage order</button></a>
                      </td>
                    </tr>';
            }
            ?>
            </tbody>
          </table>
      </div>
    </div>
    <!-- /#page-content-wrapper -->
  </div>
  <!-- /#wrapper -->
  
  <?php
    require_once 'section_footer.php';
  ?>

</body>

</html>
