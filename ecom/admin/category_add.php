<?php
require '../src/Account.php';
require '../src/Admin.php';

$account = new Account;
$admin = new Admin;
if(isset($_POST['parent_id'])){
  if($admin->add_category()){
    $message = "Category added!";
  }
}

$categories = $admin->get_category_list();
$message = '';


?>

<!DOCTYPE html>
<html lang="en">

<?php
  require 'section_head.php';
?>

<body>

  <div class="d-flex" id="wrapper">
  <?php
    require 'section_sidebar.php';
  ?>
    <!-- Page Content -->
    <div id="page-content-wrapper">
    <?php
      require 'section_navbar.php';
    ?>
        <div class="container-fluid p-3">
            <h2 class="mt-2">Add category</h2>
            <p><?=$message?></p>
            <form class="mt-3 p-3" action="" method="POST">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-row ">
                          <div class="form-group col-md-12">
                              <label for="inputState">Parent category</label>
                              <select id="inputState" class="form-control" name="parent_id">
                                <option selected value="0">Select parent category</option>
                                  <?php
                                    foreach ($categories as $key => $cat_item) {
                                      echo '<option  value="'.$cat_item['category_id'].'">'.$cat_item['category_name'].'</option>';
                                    }
                                  ?>
                              </select>
                          </div>
                          <div class="form-group col-md-12">
                              <label for="catergoryName">Catergory name</label>
                              <input type="text" class="form-control" id="catergoryName" placeholder="Catergory name" name="category_name" required>
                          </div>
                          
                        </div>
                        <button type="submit" class="btn btn-success">Add category</button>
                    </div>
                </div>
            </form>
        </div>

    </div>
    <!-- /#page-content-wrapper -->
  </div>
  <!-- /#wrapper -->
  <?php
    require 'section_footer.php';
  ?>

</body>
</html>
