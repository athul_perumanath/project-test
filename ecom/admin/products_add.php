<?php
require '../src/Account.php';
require '../src/Admin.php';

$account = new Account;
$admin = new Admin;

  $products = $admin->get_product_list();
  $message = '';
  if(isset($_POST['prod_name'])){
    if($p_id = $admin->add_product()){
      header('location: products_edit.php?prod_id='.$p_id);
      // $message = "Product added!";
    }
  }
?>
<!DOCTYPE html>
<html lang="en">
<?php
require_once 'section_head.php';
?>
<body>
  <div class="d-flex" id="wrapper">
    <!-- Sidebar -->
    <?php
    require_once 'section_sidebar.php';
  ?>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
      
      <?php
        require_once 'section_navbar.php';
      ?>

      <div class="container-fluid p-3">
        <h2 class="mt-2">Product details</h2>
        <p><?=$message?></p>
          <div class="row p-3">
            <div class="col-md-7">
                <div class="card">
                  <div class="card-header">basic details</div>
                  <div class="card-body">
                    <form action="" class="" method="POST">
                      <div class="row">
                        <div class="form-group col-md-12">
                          <label for="catergoryName">Product name</label>
                          <input type="text" class="form-control" id="catergoryName" placeholder="Product name" name="prod_name" required>
                        </div>
                        <div class="form-group col-md-6">
                          <label for="inputPassword4">Price mrp</label>
                          <input type="number" class="form-control" id="inputPassword4" placeholder="price" name="price_mrp" required>
                        </div>
                        <div class="form-group col-md-6">
                          <label for="inputPassword4">Price Selling</label>
                          <input type="number" class="form-control" id="inputPassword4" placeholder="selling price" name="price_selling" required>
                        </div>
                        
                        <div class="form-group col-md-6">
                          <label for="inputPassword4">Stock Quantity</label>
                          <input type="number" class="form-control" min="0" id="inputPassword4" placeholder="Stock quantity" name="stock_qty" required>
                        </div>
                        <div class="form-group col-md-12">
                          <label for="catergoryName">Product description</label>
                          <textarea class="form-control" name="prod_des"  rows="5"></textarea>
                        </div>
                        
                      </div>
                      <div class="row">
                        <div class="form-group col-md-6">
                          <button type="submit" class="btn btn-success pull-right">Add Product</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
            </div>
          </div>
      </div>
    </div>
    <!-- /#page-content-wrapper -->

  </div>
  <!-- /#wrapper -->
  
  <?php
    require_once 'section_footer.php';
  ?>

</body>

</html>
