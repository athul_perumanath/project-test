<?php
require '../src/Account.php';
require '../src/Admin.php';

$account = new Account;
$admin = new Admin;

if(isset($_POST['basic_details']))
{
  $basic_info = (object)$_POST; 
  $admin->update_product($_GET['prod_id'], $basic_info);
  
}

if(isset($_POST['upload_image']))
{
  $product_id = $_GET['prod_id'];
  if($url = $admin->uploader($_GET['prod_id'])){
    $admin->update_prod_image($product_id, $url);
  }
}

if(isset($_POST['delete_product']))
{
  $product_id = $_GET['prod_id'];
  $admin->delete_product($product_id);
}

if(isset($_POST['product_category']))
{
    $admin->assign_category($_POST['category'],$_GET['prod_id']);
}

if(isset($_POST['unassign']))
{
    $admin->unassign_category($_POST['cat_assign_id']);
}

$product = $admin->get_product($_GET['prod_id']);

if(!$product['prod_id']){
  header('location: products_list_all.php');
}
$categories = $admin->get_category_list();
$current_categories = $admin->get_assign_category($_GET['prod_id']);



?>

<!DOCTYPE html>
<html lang="en">

<?php
  require_once 'section_head.php';
?>

<body>
  <div class="d-flex" id="wrapper">
    <!-- Sidebar -->
    <?php
    require_once 'section_sidebar.php';
  ?>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

      <?php
        require_once 'section_navbar.php';
      ?>

      <div class="container-fluid p-3">
        <h2 class="mt-2">Product details</h2>
        <div class="row p-3">
          <div class="col-md-7">
            <div class="card">
              <div class="card-header">Basic details</div>
              <div class="card-body">
                <form action="" class="" method="POST">
                  <div class="row">
                    <div class="form-group col-md-12">
                      <label for="catergoryName">Product name</label>
                      <input type="text" class="form-control" id="catergoryName" placeholder="Product name"
                        name="prod_name" value="<?=$product['prod_name']?>" required>
                    </div>
                    <div class="form-group col-md-6">
                      <label for="inputPassword4">Price mrp</label>
                      <input type="number" class="form-control" id="inputPassword4" placeholder="price" name="price_mrp"
                        value="<?=$product['price_mrp']?>" required>
                    </div>
                    <div class="form-group col-md-6">
                      <label for="inputPassword4">Price Selling</label>
                      <input type="number" class="form-control" id="inputPassword4" placeholder="selling price"
                        name="price_selling" value="<?=$product['price_selling']?>" required>
                    </div>
                    <div class="form-group col-md-6">
                      <label for="inputPassword4">Stock Quantity</label>
                      <input type="number" class="form-control" min="0" id="inputPassword4" placeholder="Stock quantity"
                        name="stock_qty" value="<?=$product['stock_qty']?>" required>
                    </div>
                    <div class="form-group col-md-12">
                      <label for="catergoryName">Product description</label>
                      <textarea class="form-control" name="prod_des" rows="5"><?=$product['prod_des']?></textarea>
                    </div>

                  </div>
                  <div class="row">
                    <div class="form-group col-md-6">
                      <button type="submit" name="basic_details" class="btn btn-success pull-right">Update
                        Product</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div class="col-md-5">
            <div class="row mb-3">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-header">Product image</div>
                  <div class="card-body">
                    <?php
                    if($product['prod_img'] != ''){
                      echo '<img class="img-fluid" src="../'.$product['prod_img'].'" alt="">';

                    }
                    ?>
                    <form action="" method="POST" enctype="multipart/form-data">
                      <div class="row p-3">
                        <div class="form-group col-md-12">
                          <input type="file" name="prod_img" id="customFile" class="">
                        </div>
                      </div>
                      <div class="row">
                        <div class="form-group col-md-12">
                          <button type="submit" name="upload_image" class="btn btn-success pull-right">Upload
                            image</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-header">Category</div>
                  <div class="card-body">
                    <form action="" method="POST">
                      <div class="row p-3">
                        <div class="form-group col-md-12">
                          <select name="category" name="category" id="" class="form-control">
                            <?php
                              foreach($categories as $key => $category){
                                echo '<option  value="'.$category['category_id'].'">'.$category['category_name'].'</option>';
                              }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="row">
                        <div class="form-group col-md-12">
                          <button type="submit" name="product_category" class="btn btn-success pull-right">Add
                            Category</button>
                        </div>
                      </div>
                    </form>
                    <div>
                      <table class="table table-bordered mt-4 ">
                        <?php
                        foreach ($current_categories as $key => $cat) {
                          echo '<tr><td>'.$admin->get_category($cat['category_id'])['category_name'].'</td>
                          <td>
                          <form action="" method="post">
                            <input type="hidden" name="cat_assign_id" value="'.$cat['combination_id'].'">
                            <button type="submit" name="unassign" class="btn btn-danger">Remove</button></td>
                          </form>
                          </tr>';
                        }
                        ?>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12 mt-2">
                <div class="card">
                  <div class="card-header">Delete product</div>
                  <div class="card-body">
                    <form action="" method="POST">
                      <div class="row text-center">
                        <div class="form-group col-md-12">
                          <button type="submit" name="delete_product" class="btn btn-danger pull-right">Delete Product</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /#page-content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <?php
    require_once 'section_footer.php';
  ?>

</body>

</html>