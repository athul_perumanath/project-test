<?php
require '../src/Account.php';
require '../src/Admin.php';
require '../src/Shop.php';

$account = new Account;
$admin = new Admin;
$shop = new Shop;
if (isset($_POST['delete_thread'])) {
  $shop->delete_thread();
}
$threads = $shop->get_all_threads();
?>

<!DOCTYPE html>
<html lang="en">

<?php
  require_once 'section_head.php';
?>

<body>

  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <?php
      require_once 'section_sidebar.php';
    ?>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
      <?php
        require_once 'section_navbar.php';
      ?>


      <div class="container-fluid p-3">
        <h2 class="mt-2">All Threads</h2>

        <table class="table table-bordered mt-4">
            <thead>
              <tr>
                <th scope="col">ID</th>
                <th scope="col">Thread Title</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
            <?php
            foreach ($threads as $key => $thread) {
              echo '<tr>
              <th scope="row">'.$thread['id'].'</th>
              <td>'.$thread['title'].'</td>
              <td> 
              <a href="forum_thread_view.php?id='.$thread['id'].'"><button type="button" class="btn btn-primary btn-sm">Manage thread</button></a>
              <form action="" method="POST">
                    <input type="hidden" name="thread_id" value="'.$thread['id'].'">
                    <button type="submit" name="delete_thread" class="pull- btn btn-sm btn-danger">Delete</button>
                </form>
              </td>
            </tr>';
            }
            ?>
            </tbody>
          </table>
      </div>



    </div>
    <!-- /#page-content-wrapper -->

  </div>
  <!-- /#wrapper -->

    <?php
        require_once 'section_navbar.php';
      ?>

</body>

</html>
