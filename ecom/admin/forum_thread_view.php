<?php
require '../src/Account.php';
require '../src/Admin.php';
require '../src/Shop.php';

$account = new Account;
$admin = new Admin;
$shop = new Shop;

// $categories = $admin->get_category_list();
$message = '';

if(isset($_POST['delete_reply'])) {
  $shop->delete_reply();
}

if (!isset($_GET['id'])){
  header('location: forum_threads.php');
}
if(!$thread = $shop->get_thread($_GET['id'])){
  header('location: forum_threads.php');
}




?>

<!DOCTYPE html>
<html lang="en">

<?php
  require_once 'section_head.php';
?>

<body>

  <div class="d-flex" id="wrapper">

    <?php
      require_once 'section_sidebar.php';
    ?>

    <!-- Page Content -->
    <div id="page-content-wrapper">
      <?php
        require_once 'section_navbar.php';
      ?>

      <div class="container-fluid p-3">
        <h2 class="mt-2">Forum thread</h2>
        <h4 class="mt-2"><?=$thread['title']?></h4>
        <p><?=$message?></p>
        <div class="row">
          <div class="col-md-6">
            <div class="card" style="">
              <ul class="list-group list-group-flush">
                <?php
                  foreach ($thread['replies'] as $key => $reply) {
                    echo '
                    <li class="list-group-item">'.$reply['reply_message'].'
                    <form action="" method="POST">
                    <input type="hidden" name="reply_id" value="'.$reply['id'].'">
                    <div class="btn btn-sm btn-outline-primary">'.$reply['user']['full_name'].'</div>
                    <button type="submit" name="delete_reply" class="pull-right btn btn-sm btn-danger">Delete</button>
                    </form></li>';
                  }
                ?>
              </ul>
            </div>
           

          </div>
        </div>
      </div>

    </div>
    <!-- /#page-content-wrapper -->
  </div>
  <!-- /#wrapper -->

  <?php
      require_once 'section_footer.php';
    ?>
</body>

</html>