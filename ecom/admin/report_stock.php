<?php
require '../src/Account.php';
require '../src/Admin.php';

$account = new Account;
$admin = new Admin;
$products = $admin->get_product_list();
?>

<!DOCTYPE html>
<html lang="en">

<?php
  require_once 'section_head.php';
?>

<body>

  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <?php
      require_once 'section_sidebar.php';
    ?>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
      <?php
        require_once 'section_navbar.php';
      ?>


      <div class="container-fluid p-3">
        <h2 class="mt-2">Stock report</h2>

        <table class="table table-bordered mt-4">
            <thead>
              <tr>
                <th scope="col">Sl no.</th>
                <th scope="col">Product ID</th>
                <th scope="col">Product Name</th>
                <th scope="col">Price</th>
                <th scope="col">Quantity</th>
                <th scope="col">Product total</th>
                
              </tr>
            </thead>
            <tbody>
            <?php
            $total = 0;
            foreach ($products as $key => $product) {
              $warning_class = ($product['stock_qty'] < 11)?'class="bg-danger text-light"':'class=""';
              $sum = $product['price_selling'] * $product['stock_qty'];
              echo '<tr>
              <th scope="row">'.($key + 1).'</th>
              <td>'.$product['prod_id'].'</td>
              <td>'.$product['prod_name'].'</td>
              <td>'.$product['price_selling'].'</td>
              <td '.$warning_class.'>'.$product['stock_qty'].'</td>
              <th>'.$sum.'</th>
            </tr>';
            $total += $sum;
            }
            echo '<tr>
              
              <td colspan="5">Total </td>
              
              <th>'.$total.'</th>
            </tr>';
            ?>
            </tbody>
          </table>
      </div>



    </div>
    <!-- /#page-content-wrapper -->

  </div>
  <!-- /#wrapper -->

    <?php
        require_once 'section_navbar.php';
      ?>

</body>

</html>
