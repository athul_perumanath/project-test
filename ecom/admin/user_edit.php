<?php
require '../src/Account.php';
require '../src/Admin.php';

$account = new Account;
$admin = new Admin;
$message = '';
if(isset($_POST['full_name'])){
  if($admin->update_user($_GET['user_id'])){
    $message = "User Updated!";
  }
}
$user = $admin->get_user($_GET['user_id']);

?>

<!DOCTYPE html>
<html lang="en">

<?php
  require_once 'section_head.php';
?>
<body>
  <div class="d-flex" id="wrapper">
    <!-- Sidebar -->
    <?php
    require_once 'section_sidebar.php';
  ?>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

    <?php
        require_once 'section_navbar.php';
      ?>


      <div class="container-fluid p-3">
        <h2 class="mt-2">Edit user</h2>
        <p><?=$message?></p> 
          <div class="row p-3">
            <div class="col-md-7">
                <div class="card">
                  <div class="card-header">User details</div>
                  <div class="card-body">
                    <form action="" class="" method="POST">
                      <div class="row">
                        <div class="form-group col-md-12">
                          <label for="catergoryName">name</label>
                          <input type="text" class="form-control" id="catergoryName" placeholder="Enter name" name="full_name" value="<?=$user['full_name']?>" required>
                        </div>
                        <div class="form-group col-md-6">
                          <label for="inputPassword4">username</label>
                          <input type="text" class="form-control" id="inputPassword4" placeholder="Enter username" name="username" value="<?=$user['username']?>" required>
                        </div>
                        <div class="form-group col-md-6">
                          <label for="inputPassword4">Password</label>
                          <input type="password" class="form-control" id="inputPassword4" placeholder="Enter password" name="password">
                        </div>
                        <div class="form-group col-md-12">
                          <label for="inputPassword4">Email</label>
                          <input type="email" class="form-control" id="inputPassword4" placeholder="Enter email" name="email" value="<?=$user['email']?>" required>
                        </div>
                      </div>
                      <div class="row">
                        <div class="form-group col-md-6">
                          <button type="submit" class="btn btn-success pull-right">Update User</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
            </div>
            <div class="col-md-5">
              
            </div>
          </div>
      </div>

    </div>
    <!-- /#page-content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <?php
    require_once 'section_footer.php';
  ?>

</body>

</html>
