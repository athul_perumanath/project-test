<nav class="navbar navbar-expand-md bg-dark navbar-dark">
    <!-- Brand -->
    <a class="navbar-brand" href="index.php"><img src="includes/logo.jpg" alt="logo" width="60px" /></a>
    <!-- Toggler/collapsibe Button -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <!-- Navbar links -->
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="index.php">Home</a>
            </li>
            <?php
                foreach ($shop->get_categories() as $key => $category) {
                    echo '<li><a class="nav-link" href="category.php?category_id='.$category['category_id'].'">'.$category['category_name'].'</a></li>';
                }
                ?>
            <li class="nav-item">
                <a class="nav-link" href="forum.php">Forum</a>
            </li>
            
            <?php
                if(isset($_SESSION['user_id'])){
                    echo '<li class="nav-item">
                        <a class="nav-link" href="orders.php">Orders</a>
                    </li>';
                }
            ?>

        </ul>
        <ul class="navbar-nav">
            <li class="nav-item d-flex">
                <a class="nav-link" href="cart.php"><i class="fa fa-shopping-cart mr-2"></i>
                    <span><?php
                                if(isset($_SESSION['user_id'])){
                                    // $store = new Store;
                                    echo $shop->get_cart_count($_SESSION['user_id']);
                                }else{
                                    echo 0;
                                }
                                ?>
                </a>
            </li>
            <li class="nav-item">
                <!-- <a class="nav-link" href="my_account.php"><i class="fa fa-user-circle mr-2"></i>My Account</a> -->
                <?php
                        if(isset($_SESSION['user_id']) && isset($_SESSION['logined']) && isset($_SESSION['full_name'])){
                            echo '<li class="nav-item"><a class="nav-link" href="account.php"><i class="fa fa-user-o"></i> My account</a></li>';
                            echo '<li class="nav-item"><a class="nav-link" href="acc_logout.php"><i class="fa fa-user-o"></i> Logout</a></li>';
                        }else{
                            echo '<li class="nav-item"><a class="nav-link" href="acc_login.php"><i class="fa fa-user-o"></i> Login</a></li>';
                        }
                    ?>

        </ul>
    </div>
</nav>