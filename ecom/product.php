<?php
    require 'src/Account.php';	
	require 'src/Shop.php';
    $account = new Account;	
	$shop = new Shop;

	
	if(isset($_POST['add_cart'])){
		$shop->add_cart($_POST['prod_id'], $_POST['prod_qty']);
		
	}

	$product = $shop->get_product($_GET['product_id']);
	$categories = $shop->get_categories_of_product($_GET['product_id']);

?>

<?php include('./section_head.php'); ?>
<?php include('./section_header_main.php'); ?>


<!-- /NAVIGATION -->

<!-- container -->
<main class="container">
	<!-- row -->
	<div class="row mt-5">
		<!-- Product main img -->
		<div class="col-md-5 col-md-push-2">
			<div id="product-main-img">
				<div class="product-preview">
					<img src="<?=$product['prod_img']?>" alt="" class="img-fluid">
				</div>

			</div>
		</div>
		<!-- /Product main img -->


		<!-- Product details -->
		<div class="col-md-5">
			<div class="product-details">
				<h2 class="product-name"><?=$product['prod_name']?></h2>
				<div>
					<!--  -->
				</div>
				<div>
					<h3 class="product-price">$<?=$product['price_selling']?><del class="product-old-price">
							$<?=$product['price_mrp']?></del></h3>
					<span class="product-available"><?=($product['stock_qty'] > 0)?'In Stock':'Out of stock';?></span>
				</div>
				<p><?=$product['prod_des']?></p>
				<form action="" method="post">
					<input type="hidden" value="<?=$product['prod_id']?>" name="prod_id">
					<div class="add-to-cart">
						<div class="qty-label">
							Qty
							<div class="input-number">
								<input type="number" value="1" name="prod_qty">
							</div>
						</div>
						<?=($product['stock_qty'] > 0)?'<button type="submit" name="add_cart" class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> add to cart</button>':'';?>

					</div>
				</form>

				<ul class="product-links">
					<li>Category:</li>
					<?php
						foreach ($categories as $key => $category) {
							echo '<li><a href="category.php?category_id='.$category['category_id'].'">'.$shop->get_category_info($category['category_id'])['category_name'].'</a></li>';
							
						}
					?>
				</ul>


			</div>
		</div>
		<!-- /Product details -->


	</div>
	<!-- /row -->
</main>
<!-- /container -->

<!-- FOOTER -->
<?php require 'section_footer.php';?>
<!-- jQuery Plugins -->