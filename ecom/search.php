<?php
    require 'src/Account.php';	
	require 'src/Shop.php';
    $account = new Account;	
    $shop = new Shop;

    if(isset($_POST['add_cart'])){
		$product_id = $_POST['add_cart'];
		$shop->add_cart($product_id,1);
		
	}
    $products = $shop->search_result($_GET['q']);

?>
<!DOCTYPE html>
<html lang="en">
<!-- HEAD -->
<?php require 'section_head.php';?>
<!-- /HEAD -->
	<body>
		<!-- HEADER -->
		<header>
            <?php require 'section_header_main.php';?>			
                <!-- TOP HEADER --> <!-- /TOP HEADER -->
                <!-- MAIN HEADER --> <!-- /MAIN HEADER -->
		</header>
		<!-- /HEADER -->


		<!-- NAVIGATION -->
        <?php require 'section_nav.php';?>			
		
		<!-- /NAVIGATION -->

		<!-- SECTION -->
		<div class="section">
			<!-- container -->
            <div class="container">
				<!-- row -->
				<div class="row">
				<!-- STORE -->
                    <div id="store" class="col-md-12">
                    <h4 class="mt-3">Search for  "<?php echo $_GET['q'] ?>"</h4>
                        <div class="row mt-4">
                        <?php 
                        if(empty($products)){
                            echo "No results";
                        }
                            foreach ($products as $key => $product) {
                                $product_info = $shop->get_product($product['prod_id']);
                                $product_image = ($product_info['prod_img'] != '')?$product_info['prod_img']:'static/no-img.png';
                                echo '<div class="col-md-3 col-xs-6">
                                    <div class="product">
                                        <div class="product-img">
                                        <img src="'.$product_image.'" alt="" class="img-fluid"></div>
                                        <div class="product-body">
                                            <h3 class="product-name"><a href="product.php?product_id='.$product_info['prod_id'].'">'.$product_info['prod_name'].'</a></h3>
                                            <h4 class="product-price"><small><strike>$'.$product_info['price_mrp'].'</strike></small>&nbsp;$'.$product_info['price_selling'].'</h4>
                                            
                                        </div>
                                        <div class="add-to-cart">
                                            <form action="" method="post">
                                                <button type="submit" name="add_cart" value="'.$product_info['prod_id'].'" class="btn border add-to-cart-btn"><i class="fa fa-shopping-cart"></i> add to cart</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>';
                            }
                        ?>
                        
                        </div>
                    </div> 
				<!-- /STORE -->
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /SECTION -->

		<!-- FOOTER -->
        <?php require 'section_footer.php';?>
        <!-- jQuery Plugins -->

		<!-- /FOOTER -->

	</body>
</html>
