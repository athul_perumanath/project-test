<?php
	require 'src/Account.php';	
	require 'src/Shop.php';
	$account = new Account;	
	$shop = new Shop;

	if(isset($_POST['order_place'])){
	$shop->add_order();
}

   $cart = $shop->get_cart_items($_SESSION['user_id']);

?>

<?php require 'section_head.php';?>
<?php require 'section_header_main.php';?>

<main class="section">
	<form action="" method="post">
		<!-- container -->
		<div class="container">
			<!-- row -->
			<div class="row mt-4">

				<div class="col-md-7">
					<!-- Billing Details -->
					<div class="billing-details">
						<div class="section-title">
							<h3 class="title">Shipping address</h3>
						</div>
						<div class="form-group">
							<input class="input" type="text" name="full_name" placeholder="Name" required>
						</div>
						<div class="form-group">
							<input class="input" type="text" name="address" placeholder="Address" required>
						</div>
						<div class="form-group">
							<input class="input" type="tel" name="tel" placeholder="Contact number" required>
						</div>
						<div class="form-group">
							<input class="ml-2" type="radio" name="payment_mode" placeholder="" value="cod" required><label for="">COD</label>
							<input class="ml-2" type="radio" name="payment_mode" placeholder="" value="debit/credit" required><label for="">Debit/Credit card</label>
							<input class="ml-2" type="radio" name="payment_mode" placeholder="" value="paypal" required><label for="">Paypal</label>
						</div>
						
					</div>
				</div>

				<!-- Order Details -->
				<div class="col-md-5 order-details">
					<div class="section-title text-center">
						<h3 class="title">Your Order</h3>
					</div>
					<div class="order-summary">
						<div class="order-col">
							<div><strong>PRODUCT</strong></div>
							<div><strong>TOTAL</strong></div>
						</div>
						<div class="order-products">
							<?php
								$total = 0;
								$payable_amount = 0;
									foreach ($cart as $key => $item) {
									$product = $shop->get_product($item['product_id']);
									$total = ($product['price_selling'] * $item['qty']);

										echo '<div class="order-col">
												<div>'. $item['qty'].'x '.$product['prod_name'].'</div>
												<div>'.$total.'</div>
											</div>';
									$payable_amount += $total;
									}
								?>

						</div>
						<div class="order-col">
							<div>Shipping</div>
							<div><strong>50</strong></div>
							<?php	$payable_amount += 50;
								?>
						</div>
						<div class="order-col">
							<div><strong>TOTAL</strong></div>
							<div><strong class="order-total"><?=$payable_amount?></strong></div>
						</div>
					</div>
					<button type="submit" name="order_place" class="primary-btn order-submit">Place order</button>
					<!-- <a href="#" ></a> -->
				</div>
				<!-- /Order Details -->
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->
	</form>

</main>
<?php require 'section_footer.php';?>