<?php
    require 'src/Account.php';	
	require 'src/Shop.php';
    $account = new Account;	
    $shop = new Shop;
    
    if(isset($_POST['add_cart'])){
		$product_id = $_POST['add_cart'];
		$shop->add_cart($product_id,1);
		
	}
    
    $products = $shop->get_category_products($_GET['category_id']);
    $category_title = $shop->get_category_info($_GET['category_id'])['category_name'];

?>
<?php include('./section_head.php'); ?>
<?php include('./section_header_main.php'); ?>
<!DOCTYPE html>
<html lang="en">
<!-- HEAD -->
<?php require 'section_head.php';?>

<main class="container">
    <!-- row -->
    <div class="row">
        <div class="col-md-12 mt-2 mb-3">
            <div class="section-title">
                <h3 class="title"><?=$category_title?></h3>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- STORE -->
        <div id="store" class="col-md-12">
            <div class="row">
                <?php 
                    foreach ($products as $key => $product) {
                        $product_info = $shop->get_product($product['product_id']);
                        $product_image = ($product_info['prod_img'] != '')?$product_info['prod_img']:'static/no-img.png';
                        echo '<div class="col-12 col-sm-3">
                                <div class="">
                                    <div class="product-img">
                                    <img src="'.$product_image.'" alt="" width="300px" class="img-fluid></div>
                                    <div class="product-body">
                                        <h3 class="product-name"><a href="product.php?product_id='.$product_info['prod_id'].'">'.$product_info['prod_name'].'</a></h3>
                                        <h4 class="product-price"><small><strike>$'.$product_info['price_mrp'].'</strike></small>&nbsp;$'.$product_info['price_selling'].'</h4>
                                        
                                    </div>
                                    <div class="add-to-cart">
                                        <form action="" method="post">
                                            <button type="submit" name="add_cart" value="'.$product_info['prod_id'].'" class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> add to cart</button>
                                        </form>
                                    </div>
                                </div>
                            </div>';
                    }
                ?>
            </div>
        </div>
        <!-- /STORE -->
    </div>
    <!-- /row -->
</main>
<?php require 'section_footer.php';?>