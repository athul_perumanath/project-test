<?php
    require 'src/Shop.php';
    require 'src/Account.php';
    $shop = new Shop;
    $account = new Account;

    if(isset($_SESSION['user_id']) && isset($_SESSION['logined']) && isset($_SESSION['user_type'])){
	}else{
		header('location: acc_login.php');
    }
    $error = false;
    if(isset($_POST['change_pass'])){
        $password_message = $account->change_password();
    }
    if(isset($_POST['update_profile'])){
        $profile_message = $account->update_profile();
    }
    $user = $account->get_session_user();

    if(isset($_SESSION['user_id']) && isset($_SESSION['logined']) && isset($_SESSION['user_type'])){
        if($_SESSION['user_type'] == 'admin'){
            header('location: admin/order_pending.php');
        }
    }
  

?>

<?php require 'section_head.php';?>
<?php require 'section_header_main.php';?>

<main class="section">
    <!-- container -->
    <div class="container">
        <div class="row mt-4">
            <div class="col-sm-6 col-sm-offset-1">
                <div class="login-form">
                    <!--login form-->
                    <h2>Edit profile</h2>
                    <form action="" method="post">
                        <div class="form-group">
                            <label for="">Name:</label>
                            <input type="text" size="20" placeholder="Name" name="name" class="input" value="<?=$user['full_name'] ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="">Email:</label>
                            <input type="email" size="20" placeholder="Email" name="email" class="input" value="<?=$user['email'] ?>" required>
                        </div>
                        <button type="submit" name="update_profile" class="btn btn-info pull-right">Update Details</button>
                    </form>
                    <p><?php if(isset($profile_message)){ echo $profile_message;} ?></p>

                </div>
            </div>
            <div class="col-sm-6 col-sm-offset-1">
                <div class="login-form">
                    <!--login form-->
                    <h2>Change password</h2>
                    <form action="" method="post">
                        <div class="form-group">
                            <label for="">Current password:</label>
                            <input type="password" size="20" placeholder="Current Password" name="current_pass" class="input" required>
                        </div>
                        <div class="form-group">
                            <label for="">New pasword:</label>
                            <input type="password" size="20" placeholder="New Password" name="new_pass" class="input" required>
                        </div>
                        <div class="form-group">
                            <label for="">Repeat new pasword:</label>
                            <input type="password" size="20" placeholder="Repeat New Password" name="new_pass_repeat" class="input" required>
                        </div>
                        <button type="submit" name="change_pass" class="btn btn-info pull-right">Update Details</button>
                    </form>
                    <p  class="text text-danger"><?php if(isset($password_message)){ echo $password_message;} ?></p>
                </div>
            </div>

        </div>
    </div>
    <!-- /container -->
</main>
<!-- /SECTION -->

<!-- FOOTER -->
<?php require 'section_footer.php';?>