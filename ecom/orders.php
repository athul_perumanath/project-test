<?php
	require 'src/Shop.php';
    require 'src/Account.php';
    $account = new Account;	
	$shop = new Shop;

	if(isset($_SESSION['user_id']) && isset($_SESSION['logined']) && isset($_SESSION['user_type'])){
	}else{
		header('location: acc_login.php');
		
	}

	$orders = $shop->get_orders($_SESSION['user_id']);

?>

<?php require 'section_head.php';?>

<?php require 'section_header_main.php';?>

<main class="section">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row mt-3">
			<h2 class="clear-both">Orders</h2>
			<table class="table mt-2 table-responsive">
				<tr>
					<td>order id</td>
					<td>order items</td>
					<td>order date</td>
					<td>address</td>
					<td>amount</td>
					<td>order status</td>
				</tr>
				<?php
						
						$total = 0;
						$grand_total = 0;
						$checkout = true;
						foreach ($orders as $key => $order) {
							$items = $shop->order_items($order['order_id']);
							
							echo '<tr>
										<td>'.$order['order_id'].'</td>
										<td>';
							foreach ($items as $key => $item) {
								echo $key + 1;
								echo ' - <a href="product.php?product_id='.$item['product_id'].'">'.$shop->get_product($item['product_id'])['prod_name'].'</a><br>';
							}
							
							echo '</td>
										<td>'.$order['order_date'].'</td>
										<td>'.$order['delivery_address'].'</td>
										<td>'.$order['order_total'].'</td>
										<td>'.$order['order_status'].'</td>
									
								</tr>';
						}

						?>


			</table>
		</div>
		<!-- /row -->

	</div>
	<!-- /container -->
</main>
<!-- /SECTION -->
<?php require 'section_footer.php';?>