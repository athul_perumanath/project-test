<?php
    require 'src/Account.php';	
	require 'src/Shop.php';
    $account = new Account;	
	$shop = new Shop;
	if(isset($_POST['add_cart'])){
		$product_id = $_POST['add_cart'];
		$shop->add_cart($product_id,1);
		
	}
    $new_products = $shop->get_new_products();
?>
    <?php include('./section_head.php'); ?>
    <?php include('./section_header_main.php'); ?>

<main class="container-fluid">
    <div class="row">
        <div class="col-sm-12 p-0">
            <div id="demo" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ul class="carousel-indicators">
                    <li data-target="#demo" data-slide-to="0" class="active"></li>
                    <li data-target="#demo" data-slide-to="1"></li>
                    <li data-target="#demo" data-slide-to="2"></li>
                </ul>
                <!-- The slideshow -->
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="includes/home_one.jpg" alt="earphone" width="100%">
                        <div class="carousel-caption">
                            <h3>EAR PHONES</h3>
                            <p>Sound is the vocabulary of nature</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="includes/home_two.jpg" alt="soundbar" width="100%">
                        <div class="carousel-caption">
                            <h3>HEAD PHONES</h3>
                            <p>This year let great sound motivate youu</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="includes/home_three.jpg" alt="soundsystem" width="100%">
                        <div class="carousel-caption">
                            <h3>WIRELESS</h3>
                            <p>Discover freedom of a truly wireless life</p>
                        </div>
                    </div>
                </div>
                <!-- Left and right controls -->
                <a class="carousel-control-prev" href="#demo" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next" href="#demo" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- Three Promotions -->
        <div class="col-sm-12 p-2">
            <div class="col-sm-6 p-2 float-right text-center">
                <div class="col-sm-12 p-5 bg-danger float-right rcorners">
                    <p class="text-light"><small>JBL</small></p>
                    <h4 class="text-light"><b>Beats Bar 2.1</b></h4>
                    <h1 class="text-light opacity"><b>Big Bass</b></h1>
                    <div class="btn btn-light mt-3 pill">
                        <a href="shop.php" style="text-decoration: none;" class="text-danger">Shop Now</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 p-2 float-right text-center">
                <div class="col-sm-12 p-5 float-right bg-secondary rcorners"
                    style="background-image: url(includes/airpod.jpg);">
                    <p class="text-dark"><small>Enjoy</small></p>
                    <h4 class="text-dark"><b>With</b></h4>
                    <h1 class="text-dark opacity"><b>Air Pods</b></h1>
                    <div class="btn btn-light mt-3 pill">
                        <a href="shop.php" style="text-decoration: none;" class="text-danger">Shop Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Four Services -->
    <div class="row container-fluid">
        <div class="row col-6 col-sm-3">
            <div class="col-12 col-lg-4 ">
                <img src="includes/secure.jpg" alt="secure payment" class="img-fluid" width="100" />
            </div>
            <div class="col-12 col-lg-8">
                <p><b>Secure Payment</b></p>
                <p class="text-secondary">All Cards Accepted</p>
            </div>
        </div>
        <div class="row col-6 col-sm-3">
            <div class="col-12 col-lg-4 ">
                <img src="includes/support.png" alt="tech support" class="img-fluid" width="100" />
            </div>
            <div class="col-12 col-sm-8">
                <p><b>Online Support</b></p>
                <p class="text-secondary">Technical Support 24/7</p>
            </div>
        </div>
        <div class="row col-6 col-sm-3">
            <div class="col-12 col-lg-4 ">
                <img src="includes/money.jpg" alt="money back" class="img-fluid" width="100" />
            </div>
            <div class="col-12 col-lg-8">
                <p><b>Money Guarantee</b></p>
                <p class="text-secondary">30 Day Money Back</p>
            </div>
        </div>
        <div class="row col-6 col-sm-3">
            <div class="col-12 col-lg-4 ">
                <img src="includes/delivery.png" alt="money back" class="img-fluid" width="100" />
            </div>
            <div class="col-12 col-lg-8">
                <p><b>Free Shipping</b></p>
                <p class="text-secondary">Free Shipping On All Order</p>
            </div>
        </div>
    </div>

    <div class="row container-fluid rcorners bg-success m-0 ">
        <div class="col-12 col-sm-5 p-0 m-0">
            <p class="text-light"><small><b>Beats</b></small></p>
            <h1 class="text-light"><b>Summer <br> Sale</b></h1>
            <p class="text-light"><small>Great Discount!</small></p>
            <div class="btn btn-light mt-3 pill">
                <a href="shop.php" style="text-decoration: none;" class="text-success">Shop Now</a>
            </div>
        </div>
        <div class="col-12 col-sm-7 p-1">
            <p class="text-light"><b>20 % OFF</b></p>
            <p class="text-light display-2   font-weight-bold">HAPPY <br> HOURS</p>
            <p class="text-light"><b>Every Friday</b></p>
        </div>
    </div>

</main>


<?php include('./section_footer.php'); ?>