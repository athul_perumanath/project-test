<?php
// require '../conf/database.php';
class Admin{

    public function __construct()
    {
        $database = (object)array('host' => 'localhost','username' => 'root','password' => '','database' => 'db_ecomm');
        $this->dbConnect = mysqli_connect($database->host, $database->username, $database->password, $database->database);
        // print_r(mysqli_connect_error($this->dbConnect));
        if(isset($_SESSION['user_id']) && isset($_SESSION['logined']) && isset($_SESSION['user_type'])){
            if($_SESSION['user_type'] != 'admin'){
                header('location: ../index.php');
            }
        }else{
            header('location: ../index.php');
        }
        
    }


// User management
    public function get_users_list()
    {
        $sql = "SELECT * FROM `ecom_users` WHERE 1";
        return mysqli_fetch_all(mysqli_query($this->dbConnect, $sql), MYSQLI_ASSOC);
    }

    public function get_user($user_id)
    {
        $sql = "SELECT * FROM `ecom_users` WHERE `user_id` = '$user_id'";
        return mysqli_fetch_assoc(mysqli_query($this->dbConnect, $sql));
    }

    public function add_user()
    {
        $full_name = $_POST['full_name'];
        $username = $_POST['username'];
        $password = $_POST['password'];
        $email = $_POST['email'];
        $hash = password_hash($password , PASSWORD_DEFAULT);
        $sql = "INSERT INTO `ecom_users` (username,password,email,full_name) VALUES ('$username','$hash','$email','$full_name')";
        if(mysqli_query($this->dbConnect, $sql)){
            return true;
        }
        return false;
    }

    public function update_user($user_id)
    {
      $full_name = $_POST['full_name'];
      $username = $_POST['username'];
      $email = $_POST['email'];

      $sql = "UPDATE `ecom_users` SET `username`='$username',   `email` = '$email' , `full_name` = '$full_name' WHERE `user_id` = '$user_id'";

      if($_POST['password'] != ''){
        $password = $_POST['password'];
        $hash = password_hash($password , PASSWORD_DEFAULT);
        $sql = "UPDATE `ecom_users` SET `username`='$username' , `password` = '$hash' , `email` = '$email' , `full_name` = '$full_name' WHERE user_id = $user_id";
      }

      if(mysqli_query($this->dbConnect, $sql)){
        return true;
        }
        return false;
    }

// Order management
    public function get_order_list($status = 'pending')
    {//pending / 
        $sql = "SELECT * FROM `orders` WHERE `order_status` = '$status' ORDER BY `order_id` DESC";
        //processing / shipped / return 
        if($status == 'processing'){
            $sql = "SELECT * FROM `orders` WHERE `order_status` = 'processing' OR `order_status` = 'shipped' OR `order_status` = 'return' ORDER BY `order_id` DESC";
        }
        //completed / refunded
        if($status == 'completed'){
            $sql = "SELECT * FROM `orders` WHERE `order_status` = 'completed' OR `order_status` = 'refunded' ORDER BY `order_id` DESC";
        }
        
        return mysqli_fetch_all(mysqli_query($this->dbConnect, $sql), MYSQLI_ASSOC);
        // print_r(mysqli_error($this->dbConnect));
    }

    public function get_order($order_id)
    {
        $sql = "SELECT * FROM `orders` WHERE `order_id` = '$order_id'";
        $order = mysqli_fetch_assoc(mysqli_query($this->dbConnect, $sql));
        if(!$order){
            return false;
        }
        $user = $this->get_user($order['user_id']);
        $order_items = $this->order_items($order_id);
        $payment = $this->get_payments($order_id);

        $order['customer'] = $user['full_name'];
        $order['order_items'] = $order_items;
        $order['payment'] = $payment;
    

        return $order;
    } 

    public function update_order($order_id)
    {
      $order_status = $_POST['order_status'];
      $tracking_id = $_POST['tracking_id'];
      $sql = "UPDATE `orders` SET `order_status` = '$order_status' , `tracking_id` = '$tracking_id' WHERE order_id = $order_id";
      if(mysqli_query($this->dbConnect, $sql)){
        return true;
    }
    return false;

    }

    public function order_items($order_id)
    {
        $sql = "SELECT * FROM `order_item` WHERE `order_id` = '$order_id'";
        $items = mysqli_fetch_all(mysqli_query($this->dbConnect, $sql), MYSQLI_ASSOC);
        foreach ($items as $key => $item) {
            $items[$key]['product_name'] = $this->get_product($item['product_id'])['prod_name'];
        }
        return $items;

    }

    public function get_payments($order_id)
    {
        $sql = "SELECT * FROM `payments` WHERE `order_id` = '$order_id'";
        return mysqli_fetch_assoc(mysqli_query($this->dbConnect, $sql));
    }

    public function get_orders_count($status = 'pending')
    {//pending / processing / shipped / completed / cancelled
        $sql = "SELECT * FROM `orders` WHERE `order_status` = '$status'";
        return mysqli_num_rows(mysqli_query($this->dbConnect, $sql));
    }

// Product management

    public function get_product_list()
    {
        $sql = "SELECT * FROM `products` WHERE `deleted` = '0'";
       return mysqli_fetch_all(mysqli_query($this->dbConnect, $sql), MYSQLI_ASSOC);
      
    }

    
    public function add_product()
    {
      $prod_name = $_POST['prod_name'];
      $price_mrp = $_POST['price_mrp'];
      $price_selling = $_POST['price_selling'];
      $stock_qty = $_POST['stock_qty'];
      $prod_des = $_POST['prod_des'];
     
    $sql = "INSERT INTO `products` (prod_name,price_mrp,price_selling,stock_qty, prod_des) VALUES ('$prod_name','$price_mrp','$price_selling','$stock_qty', '$prod_des')"; 

        if(mysqli_query($this->dbConnect, $sql)){
            return mysqli_insert_id($this->dbConnect);
        }
        return false;

    }

    public function get_product($product_id)
    {
        $sql = "SELECT * FROM `products` WHERE `prod_id` = '$product_id'";
        return mysqli_fetch_assoc(mysqli_query($this->dbConnect, $sql));
    }

    public function update_product($product_id, $product)
    {	
        // category_id
        $sql = "UPDATE `products` SET `prod_name` = '$product->prod_name', `prod_des` = '$product->prod_des', `price_mrp` = '$product->price_mrp', `price_selling` = '$product->price_selling', `stock_qty` = '$product->stock_qty' WHERE `prod_id` = '$product_id'";
        mysqli_query($this->dbConnect, $sql);
    }

    public function delete_product($product_id)
    {	
        $sql = "DELETE FROM `products` WHERE `prod_id` = '$product_id'";
        mysqli_query($this->dbConnect, $sql);
        $sql = "DELETE FROM `product_cat_assign` WHERE `product_id` = '$product_id'";
        mysqli_query($this->dbConnect, $sql);
        $sql = "DELETE FROM `cart` WHERE `product_id` = '$product_id'";
        mysqli_query($this->dbConnect, $sql);
    }

    public function assign_category($category_id, $product_id)
    {
        $sql = "INSERT INTO `product_cat_assign` (category_id,product_id) VALUES ('$category_id','$product_id')";
        mysqli_query($this->dbConnect, $sql);
    }

    public function unassign_category($combination_id)
    {
        $sql = "DELETE FROM `product_cat_assign` WHERE `combination_id` = '$combination_id'";
        mysqli_query($this->dbConnect, $sql);
    }

    
    public function get_assign_category($product_id)
    {
        $sql = "SELECT *  FROM `product_cat_assign` WHERE `product_id` = '$product_id'";
        return mysqli_fetch_all(mysqli_query($this->dbConnect, $sql), MYSQLI_ASSOC);
    }

    public function uploader($product_id)
    {
        $upload_dir = "../uploads/";
        // $target_file_name = basename($_FILES["prod_img"]["name"]);
        $imageFileType = strtolower(pathinfo(basename($_FILES["prod_img"]["name"]),PATHINFO_EXTENSION));
        $target_file = $upload_dir.$product_id.'.'.$imageFileType;
        $uploadOk = 1;

        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
            $uploadOk = 0;
        }
        if ($uploadOk == 0) {
        }else {
            if (move_uploaded_file($_FILES["prod_img"]["tmp_name"], $target_file)) {
                return 'uploads/'.basename($target_file);
            }
        }
        return false;

    }

    public function update_prod_image($product_id, $img_url)
    {
       $sql = "UPDATE `products` SET `prod_img` = '$img_url' WHERE `prod_id` = '$product_id'";
       mysqli_query($this->dbConnect, $sql);

    }

// Category management
    public function get_category_list()
    {
        $sql = "SELECT * FROM `categories` WHERE 1";
        $categories = (mysqli_fetch_all(mysqli_query($this->dbConnect, $sql), MYSQLI_ASSOC));
        $list = array();
        foreach ($categories as $key => $category) {
            $parent_id = $category['parent_id'];
            $category['category_name_real'] = $category['category_name'];
            while($parent_id != 0){
                $new_parent = $this->get_category($parent_id);
                $new_name= $new_parent['category_name'].' > '.$category['category_name'];
                $category['category_name'] = $new_name;
                $parent_id = $new_parent['parent_id'];
            }
            array_push($list, $category);
        }
        return $list;
    }


    public function add_category()
    {
        $category_name = $_POST['category_name'];
        $parent_id = $_POST['parent_id'];
        
        $sql = "INSERT INTO `categories` (parent_id, category_name) VALUES ('$parent_id','$category_name')";
        if(mysqli_query($this->dbConnect, $sql)){
            return true;
        }
        return false;


    }

    public function get_category($category_id)
    {
        $sql = "SELECT * FROM `categories` WHERE `category_id` = '$category_id'" ;
        return mysqli_fetch_assoc(mysqli_query($this->dbConnect, $sql));
    }

    public function update_category($category_id)
    {
        $parent_id = $_POST['parent_id'];
        $category_name = $_POST['category_name'];
        $sql = "UPDATE `categories` SET parent_id = $parent_id,`category_name` = '$category_name'  WHERE category_id = $category_id" ;
        if(mysqli_query($this->dbConnect, $sql)){
            return true;
        }
        return false;
    }
    
//Report generation
    
    public function report_sales()
    {
        $sql = "SELECT * FROM `payments` WHERE `payment_status` = 'success'" ;
        return mysqli_fetch_all(mysqli_query($this->dbConnect, $sql), MYSQLI_ASSOC);
        

    }

    

    
}
