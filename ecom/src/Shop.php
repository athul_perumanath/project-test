<?php

class Shop{

    public function __construct()
    {
        $database = (object)array('host' => 'localhost','username' => 'root','password' => '','database' => 'db_ecomm');        
        $this->dbConnect = mysqli_connect($database->host, $database->username, $database->password, $database->database);
    }

    public function get_poducts()
    {
        $sql = "SELECT * FROM `products` WHERE 1";
        return mysqli_fetch_all(mysqli_query($this->dbConnect, $sql), MYSQLI_ASSOC);
    }

    public function get_product($product_id)
    {
        $sql = "SELECT * FROM `products` WHERE `prod_id` = '$product_id'";
        return mysqli_fetch_assoc(mysqli_query($this->dbConnect, $sql));
    }

    public function get_categories()
    {
        $sql = "SELECT * FROM `categories` WHERE `parent_id` = '0'";
        return mysqli_fetch_all(mysqli_query($this->dbConnect, $sql), MYSQLI_ASSOC);
    }
    
    public function get_category_info($category_id)
    {
        $sql = "SELECT * FROM `categories` WHERE `category_id` = '$category_id'";
        return mysqli_fetch_assoc(mysqli_query($this->dbConnect, $sql));
    }

    public function get_category_products($category_id)
    {
        $sql = "SELECT * FROM product_cat_assign WHERE  category_id = $category_id";
        return mysqli_fetch_all(mysqli_query($this->dbConnect, $sql), MYSQLI_ASSOC);
    }

    public function get_categories_of_product($product_id)
    {
        $sql = "SELECT * FROM product_cat_assign WHERE  product_id = $product_id";
        return mysqli_fetch_all(mysqli_query($this->dbConnect, $sql), MYSQLI_ASSOC);
    }


    public function add_cart($product_id, $quantity)
    {
        if(isset($_SESSION['user_id']) && isset($_SESSION['logined'])){
            $user_id = $_SESSION['user_id'];
            $sql = "INSERT INTO cart (user_id, product_id, qty) VALUES ('$user_id', '$product_id', '$quantity' )";
            mysqli_query($this->dbConnect, $sql);
        }else{
            header('location: acc_login.php');
        }
    }

    public function update_cart($cart_item_id, $quantity)
    {
        $sql = "UPDATE `cart` SET `qty` = '$quantity' WHERE `cart_item_id` = '$cart_item_id'";
        mysqli_query($this->dbConnect, $sql);

    }

    public function remove_cart($cart_item_id)
    {
        $sql = "DELETE FROM `cart` WHERE `cart_item_id` = '$cart_item_id'";
        mysqli_query($this->dbConnect, $sql);
       
    }

    public function get_cart_items($user_id)
    {
        $sql = "SELECT * FROM `cart` WHERE `user_id` = '$user_id'";
        return mysqli_fetch_all(mysqli_query($this->dbConnect, $sql), MYSQLI_ASSOC);
    }
    public function get_cart_count($user_id)
    {
        $sql = "SELECT * FROM `cart` WHERE `user_id` = '$user_id'";
        return mysqli_num_rows(mysqli_query($this->dbConnect, $sql));
    }

    public function get_new_products()
    {
        $sql = "SELECT * FROM `products` ORDER BY `prod_id` DESC LIMIT 6";
        return mysqli_fetch_all(mysqli_query($this->dbConnect, $sql), MYSQLI_ASSOC);
    }

    public function add_order()
    {
        //Shipping charge is Pre-defined
        $shipping_charge = 50;
        $amount = 0;
        $name = $_POST['full_name'];
        $payment_mode = $_POST['payment_mode'];
        $address = $_POST['address'].' , '.$_POST['tel'];
        $user_id = $_SESSION['user_id'];
        $shipping_method = 'Express';
        
        $cart_items = $this->get_cart_items($_SESSION['user_id']);
        print_r($cart_items);
        $sql = "INSERT INTO orders (user_id, shipping_charge, shipping_method, order_status, delivery_address) VALUES ('$user_id', '$shipping_charge', '$shipping_method', 'pending',  '$address')";
        mysqli_query($this->dbConnect, $sql);
        $order_id = mysqli_insert_id($this->dbConnect);
        foreach ($cart_items as $key => $order_item) {
            $selling_price = $this->get_product($order_item['product_id'])['price_selling'];
            $total = $selling_price * $order_item['qty'];
            $this->add_order_item($order_id, $order_item['product_id'], $selling_price, $order_item['qty'], $total);
            $amount += $total;
        }
        $amount += $shipping_charge;
        $sql = "UPDATE orders SET `order_total` = '$amount' WHERE `order_id` =  '$order_id'";
        mysqli_query($this->dbConnect, $sql);  
        $payment_id = $this->add_payment($order_id, $amount, $payment_mode);     
        header('location: payment.php?payment_id='.$payment_id);
        
    }

    function current_stock($product_id)
    {
        $sql = "SELECT * FROM `products` WHERE `prod_id` = '$product_id'";
        return mysqli_fetch_assoc(mysqli_query($this->dbConnect, $sql))['stock_qty'];
    }

    public function add_order_item($order_id, $product_id, $price, $qty, $total)
    {
        $sql = "INSERT INTO order_item (order_id, product_id, selling_price, quantity, total) VALUES ('$order_id', '$product_id', '$price', '$qty', '$total' )";
        mysqli_query($this->dbConnect, $sql);  
        $updated_stock = $this->current_stock($product_id) - $qty;
        $sql = "UPDATE products SET `stock_qty` = '$updated_stock' WHERE `prod_id` =  '$product_id'";
        mysqli_query($this->dbConnect, $sql);  
    }

    public function order_rollback($order_id)
    {
        $sql = "SELECT * FROM `order_item` WHERE `order_id` = '$order_id'";
        $items = mysqli_fetch_all(mysqli_query($this->dbConnect, $sql), MYSQLI_ASSOC);
        foreach ($items as $key => $item) {
            $this->restore_canceled_items($item['product_id'],$item['quantity']);
        }
    }

    public function restore_canceled_items($product_id, $qty)//Restore stock items for canceled/failed orders
    {
        $updated_stock = $this->current_stock($product_id) + $qty;
        $sql = "UPDATE products SET `stock_qty` = '$updated_stock' WHERE `prod_id` =  '$product_id'";
        mysqli_query($this->dbConnect, $sql);
    }


    public function add_payment($order_id, $amount, $payment_mode)
    {
        $sql = "INSERT INTO payments (order_id, amount, payment_ref) VALUES ('$order_id', '$amount', '$payment_mode')";
        mysqli_query($this->dbConnect, $sql);
        return mysqli_insert_id($this->dbConnect);  
        // print_r(mysqli_error($this->dbConnect)); 
    }

    public function txn_update($txn_id, $status)
    {
        $sql = "UPDATE payments SET `payment_status` = '$status' WHERE `transaction_id` = '$txn_id'";
        mysqli_query($this->dbConnect, $sql);
    }

    public function order_update($oid, $status)
    {
        $sql = "UPDATE orders SET `order_status` = '$status' WHERE `order_id` = '$oid'";
        mysqli_query($this->dbConnect, $sql);
    }

    public function order_id_payment_id($payment_id)
    {
        $sql = "SELECT * FROM `payments` WHERE `transaction_id` = '$payment_id'";
        return mysqli_fetch_assoc(mysqli_query($this->dbConnect, $sql))['order_id'];
    }

    
    public function search_result($query)
    {
        $search = preg_replace("#[^0-9a-z]i#","", $query);
        $sql = "SELECT * FROM products WHERE prod_name LIKE '%$search%'";
        return mysqli_fetch_all(mysqli_query($this->dbConnect, $sql), MYSQLI_ASSOC);
        
    }

    public function get_orders($user_id)
    { 
        $sql = "SELECT * FROM `orders` WHERE `user_id` = '$user_id' ORDER BY `order_id` DESC";     
        return mysqli_fetch_all(mysqli_query($this->dbConnect, $sql), MYSQLI_ASSOC);
    }

    public function order_items($order_id)
    {
        $sql = "SELECT * FROM `order_item` WHERE `order_id` = '$order_id'";
        $items = mysqli_fetch_all(mysqli_query($this->dbConnect, $sql), MYSQLI_ASSOC);
        foreach ($items as $key => $item) {
            $items[$key]['product_name'] = $this->get_product($item['product_id'])['prod_name'];
        }
        return $items;
    }

    public function get_user($user_id)
    {
        $sql = "SELECT * FROM `ecom_users` WHERE `user_id` = '$user_id'";
        return mysqli_fetch_assoc(mysqli_query($this->dbConnect, $sql));
    }

    public function add_thread($thread, $user_id)
    {
        $sql = "INSERT INTO `threads` (title,user_id) VALUES ('$thread','$user_id')";
        if(mysqli_query($this->dbConnect, $sql)){
            return true;
        }
    }

    public function get_all_threads()
    {
        $sql = "SELECT * FROM `threads` ORDER BY `id` DESC";
        $items = mysqli_fetch_all(mysqli_query($this->dbConnect, $sql), MYSQLI_ASSOC);
        foreach ($items as $key => $item) {
            $items[$key]['user'] = $this->get_user($item['user_id']);
        }
        return $items;
    }

    public function get_thread($thread_id)
    {
        $sql = "SELECT * FROM `threads` WHERE `id` = '$thread_id'";
        $thread = mysqli_fetch_assoc(mysqli_query($this->dbConnect, $sql));
        $thread['replies'] = $this->thread_replies($thread['id']);
        return $thread;
    }

    public function thread_replies($thread_id)
    {
        $sql = "SELECT * FROM `thread_replies` WHERE `thread_id` = '$thread_id' ORDER BY `id` DESC";
        $items = mysqli_fetch_all(mysqli_query($this->dbConnect, $sql), MYSQLI_ASSOC);
        foreach ($items as $key => $item) {
            $items[$key]['user'] = $this->get_user($item['user_id']);
        }
        return $items;
    }

    public function add_reply($thread_id, $reply, $user_id)
    {
        $sql = "INSERT INTO `thread_replies` (thread_id,reply_message,user_id) VALUES ('$thread_id','$reply','$user_id')";
        if(mysqli_query($this->dbConnect, $sql)){
            return true;
        }
    }

    public function delete_reply()
    {
        $id = $_POST['reply_id'];
        $sql = "DELETE FROM `thread_replies` WHERE `id` = '$id'";
        mysqli_query($this->dbConnect, $sql);
    }
    public function delete_thread()
    {
        $id = $_POST['thread_id'];
        $sql = "DELETE FROM `threads` WHERE `id` = '$id'";
        mysqli_query($this->dbConnect, $sql);
        $sql = "DELETE FROM `thread_replies` WHERE `thread_id` = '$id'";
        mysqli_query($this->dbConnect, $sql);
    }



}