
<?php
// include '../conf/database.php';  

class Account{

    public function __construct()
    {
        $database = (object)array('host' => 'localhost','username' => 'root','password' => '','database' => 'db_ecomm');
        $this->dbConnect = mysqli_connect($database->host, $database->username, $database->password, $database->database);
        session_start();

    }

    public function is_user_logined()
    {
        if(isset($_SESSION['user_id']) && isset($_SESSION['logined'])){
            return true;
        }
        return false;
    }

    public function get_session_user()
    {
        if ($this->is_user_logined()) {
            $user_id = $_SESSION['user_id'];
            $sql = "SELECT * FROM `ecom_users` WHERE `user_id` = '$user_id'";
            return mysqli_fetch_assoc(mysqli_query($this->dbConnect, $sql));
        }
    }
    
    //Login form post action will process here
    public function login()
    {
        $username = $_POST['username'];
        $password = $_POST['password'];
        $sql = "SELECT * FROM `ecom_users` WHERE `username` = '$username'";
        if($userdata = mysqli_fetch_assoc(mysqli_query($this->dbConnect, $sql))){
        //User data returned
            $hash = $userdata['password'];
            if (password_verify($password, $hash)) {
                //Login success
                //clear all session data firct
                // session_unset();
                // session_destroy();
                //set new session data
                $_SESSION['user_id'] = $userdata['user_id'];
                $_SESSION['logined'] = true;
                $_SESSION['user_type'] = $userdata['user_type'];
                $_SESSION['full_name'] = $userdata['full_name'];
                return true; 
            }
        }
        return false;
    }
    

    public function forgot_action()
    {
        
    }

    public function user_registration()
    {
        $message = '';
        $full_name = $_POST['full_name'];
        $username = $_POST['email'];
        $password = password_hash($_POST['password'] , PASSWORD_DEFAULT);
        if(!$this->is_userexists($username)){
            $sql = "INSERT INTO `ecom_users` (username, password, email, full_name) VALUES ('$username','$password', '$username', '$full_name')";
            if(mysqli_query($this->dbConnect, $sql)){
                $message = "User sign up successful, login to continue";
            }else{
                // $message = mysqli_error($this->dbConnect);
                $message = "Something went wrong, try again";
            }
        }else{
            $message = "User already exists!";
        }
        return $message;
    }
    public function is_userexists($username)
    {
        $sql = "SELECT * FROM `ecom_users` WHERE `username` = '$username'";
        if(mysqli_num_rows(mysqli_query($this->dbConnect, $sql)) > 0){
            return true;
        }else{
            return false;
        }
        
    }

    public function logout()
    {
        session_unset();
        session_destroy();
    }

    public function change_password()
    {
        if($this->is_user_logined()){
            $user_id= $_SESSION['user_id'];
            $current_pass = $_POST['current_pass'];
            $new_pass = $_POST['new_pass'];
            $new_pass_repeat = $_POST['new_pass_repeat'];
            $new_hash = password_hash($new_pass , PASSWORD_DEFAULT);

            if(password_verify($current_pass, $this->get_session_user()['password'])){
                if ($new_pass == $new_pass_repeat) {
                    $sql = "UPDATE ecom_users SET `password` = '$new_hash' WHERE `user_id` = '$user_id'";
                    mysqli_query($this->dbConnect, $sql);
                    return "password changed!";
                }else{
                    return "New password and repeat password missmatch.";
                }
            }else{
                return "Current password is wrong!";
            }
        }
    }

    public function update_profile()
    {
        if($this->is_user_logined()){
            $user_id= $_SESSION['user_id'];
            $name = $_POST['name'];
            $mail = $_POST['email'];
            $sql = "UPDATE ecom_users SET `full_name` = '$name', `email` = '$mail' WHERE `user_id` = '$user_id'";
            if(mysqli_query($this->dbConnect, $sql)){
                return "Profile updated";
            }else{
                return '<span class="text-danger">Something went wrong while updating profile</span>';
                
            }     
        }
    }


}
