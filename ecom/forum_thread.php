<?php
    require 'src/Account.php';	
	require 'src/Shop.php';
    $account = new Account;	
	$shop = new Shop;
	if(!isset($_GET['thread'])){
		header('location: forum.php');
	}
	
	if(isset($_SESSION['user_id']) && isset($_SESSION['logined']) && isset($_SESSION['user_type'])){
		$user_logined = true;
		if(isset($_POST['add_reply'])){
			$shop->add_reply($_GET['thread'], $_POST['reply_message'], $_SESSION['user_id']);
		}
	}else{
		$user_logined = false;
		
	}
	
	$thread = $shop->get_thread($_GET['thread']);
	if(!$thread['id']){
		header('location: forum.php');
	}
	// return $thread;
	// $categories = $shop->get_categories_of_product($_GET['product_id']);

?>

<?php include('./section_head.php'); ?>
<?php include('./section_header_main.php'); ?>


<!-- /NAVIGATION -->

<!-- container -->
<main class="container">
	<!-- row -->
	<div class="row">
		<div class="col-sm-12 mt-2">

			<h2 class="mt-3"><?php echo $thread['title']?></h2>
			<?php if($user_logined){ ?>
			<div class="row">
				<div class="col-sm-6 mt-2">
					<h4 class="mt-3">Add reply</h4>

					<form action="" method="POST">
						<div class="row">
							<div class="col-12">
							</div>
						</div>
						<div class="form-group">
							<input type="text" name="reply_message" placeholder="Enter Content" class="input">
						</div>
						<button type="submit" class="btn pull-right" name="add_reply">Add new thread</button>
					</form>

				</div>
			</div>
			<?php } ?>
			<!-- Product main img -->
			<ul class="list-group mt-2">

				<?php foreach($thread['replies'] as $key => $reply){?>
				<li class="list-group-item d-flex justify-content-between align-items-center">
					<?php echo $reply['reply_message'] ?>
					<span class="badge badge-primary badge-pill"><?php echo $reply['user']['full_name'] ?></span>
				</li>
				<?php } ?>
			</ul>
		</div>





	</div>
	<!-- /row -->
</main>
<!-- /container -->

<!-- FOOTER -->
<?php require 'section_footer.php';?>
<!-- jQuery Plugins -->